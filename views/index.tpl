<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Wall Street Mania</title>
%include head
<script type='javascript'>
	document.addEventListener("DOMContentLoaded", function(){
		document.getElementById('sub').addEventListener('submit', getStock, false);
	}, false);
	
	function getStock(event) {
		event.preventDefault();
		event.stopPropagation();
		reDir();
		return false;
	}
	
	function reDir() {
		var stock = document.getElementById('t').value;
		window.location = 'http://localhost:8080/ticker/'+stock;
	}
</script>
</head>
<body><div id="main">
<h1>Wall Street Mania</h1>
<p>Please type a ticker symbol below to see its current market price.</p>
<form>
	<label>Ticker:</label>
	<input type='search' name='t' id='t' pattern='[A-Z0-9]{1,4}(\.[A-Z]{2})?' \>
	<button type='submit' id='sub'>Submit</button>
</form>
 
</div></body>
</html>
